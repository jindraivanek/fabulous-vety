﻿// Copyright 2018 Fabulous contributors. See LICENSE.md for license.
namespace FabulousApp

open System
open Fabulous.Core
open Fabulous.DynamicViews
open Xamarin.Forms

module Utils =
    let memoizeBy (f: 'a -> 'b) (g: 'a -> 'c) =
        let cache = System.Collections.Concurrent.ConcurrentDictionary<_, _>(HashIdentity.Structural)
        fun x ->
            cache.GetOrAdd(Some (g x), lazy (f x)).Force()
        
    let inline memoize f = memoizeBy f id
    let tryParseInt s =
        let (success, x) = System.Int32.TryParse s
        if success then Some x else None
    let rec fb = memoize <| fun (n, xs) ->
        match xs with
        | a :: xs when a >= n -> xs
        | a :: b :: xs -> fb (n, a+b :: a :: b :: xs)
        | _ -> []
    let fib = memoize <| fun n -> fb (n, [2;1]) |> List.toArray |> Array.rev
open Utils

module App = 
    type View with 
        static member ScrollingContentPage(title, children) =
            View.ContentPage(title=title, content=View.ScrollView(View.StackLayout(padding=20.0, children=children) ), useSafeArea=true)

        static member NonScrollingContentPage(title, children, ?gestureRecognizers) =
            View.ContentPage(title=title, content=View.StackLayout(padding=20.0, children=children, ?gestureRecognizers=gestureRecognizers), useSafeArea=true)  

    open System

    type Model = 
      { Count : int
        Lines : string list
        SetLines : bool
      }

    type Msg = 
        | Increment 
        | Decrement 
        | Reset
        | SetValue of int
        | EditLines
        | SetLines of string list

    let initModel = { Count = 1; Lines = List.map string [1..1]; SetLines = false }

    let init () = initModel, Cmd.none

    let update msg model =
        match msg with
        | Increment -> { model with Count = model.Count + 1 }, Cmd.none
        | Decrement -> { model with Count = model.Count - 1 }, Cmd.none
        | Reset -> init ()
        | SetValue n -> { model with Count = n }, Cmd.none
        | EditLines -> { model with SetLines = true }, Cmd.none
        | SetLines x -> { model with Lines = x; SetLines = false; Count = max model.Count (List.length x) }, Cmd.none

    let view (model: Model) dispatch =
        let horizontalButtons buttons = View.StackLayout(orientation = StackOrientation.Horizontal, children = buttons)
                
        let editContent() =
            let mutable text = String.concat System.Environment.NewLine model.Lines
            let toLines (text: string) = text.Split([|System.Environment.NewLine|], StringSplitOptions.RemoveEmptyEntries) |> Seq.toList
            let e = View.Editor(text = text, textChanged = (fun args -> text <- args.NewTextValue), horizontalOptions = LayoutOptions.FillAndExpand)
            View.NonScrollingContentPage("edit", [ 
                View.Button(text = "Clear", horizontalOptions = LayoutOptions.Center, command = (fun () -> dispatch <| Reset))
                View.ScrollView e
                View.Button(text = "Save", horizontalOptions = LayoutOptions.Center, command = (fun () -> dispatch <| SetLines (toLines text)))
            ])
        let content() = 
            let lines = model.Lines |> List.toArray
            let selectedLines = 
                let n =  model.Count
                fib n |> Array.map (fun i -> n-i+1, lines.[(n-i)]) |> Array.toList
            View.ContentPage(content = 
            View.StackLayout(padding = 20.0, verticalOptions = LayoutOptions.Center, 
            children = [ 
                View.Label(text = sprintf "%d" model.Count)
                View.Slider(minimum = 0.0, maximum = double (Array.length lines), value = double model.Count, valueChanged = (fun args -> dispatch (SetValue (int (args.NewValue + 0.5)))), horizontalOptions = LayoutOptions.FillAndExpand)
                horizontalButtons [
                    View.Button(text = "+", command = (fun () -> dispatch Increment), horizontalOptions = LayoutOptions.Center)
                    View.Button(text = "-", command = (fun () -> dispatch Decrement), horizontalOptions = LayoutOptions.Center)
                ] ]
                @ [ (View.ScrollView (View.StackLayout(selectedLines |> List.map (fun (i,s) -> View.Label(text = sprintf "(%i) %s" i s))))) ]
                @ [ View.Button(text = "Edit", horizontalOptions = LayoutOptions.Center, command = (fun () -> dispatch EditLines)) ]
            ))
        if model.SetLines then editContent() else content()

    // Note, this declaration is needed if you enable LiveUpdate
    let program = Program.mkProgram init update view

type App () as app = 
    inherit Application ()

    let runner = 
        App.program
#if DEBUG
        |> Program.withConsoleTrace
#endif
        |> Program.runWithDynamicView app

#if DEBUG
    // Uncomment this line to enable live update in debug mode. 
    // See https://fsprojects.github.io/Fabulous/tools.html for further  instructions.
    //
    do runner.EnableLiveUpdate()
#endif    

    // Uncomment this code to save the application state to app.Properties using Newtonsoft.Json
    // See https://fsprojects.github.io/Fabulous/models.html for further  instructions.
//#if APPSAVE
    let modelId = "model"
    override __.OnSleep() = 

        let json = Newtonsoft.Json.JsonConvert.SerializeObject(runner.CurrentModel)
        Console.WriteLine("OnSleep: saving model into app.Properties, json = {0}", json)

        app.Properties.[modelId] <- json

    override __.OnResume() = 
        Console.WriteLine "OnResume: checking for model in app.Properties"
        try 
            match app.Properties.TryGetValue modelId with
            | true, (:? string as json) -> 

                Console.WriteLine("OnResume: restoring model from app.Properties, json = {0}", json)
                let model = Newtonsoft.Json.JsonConvert.DeserializeObject<App.Model>(json)

                Console.WriteLine("OnResume: restoring model from app.Properties, model = {0}", (sprintf "%0A" model))
                runner.SetCurrentModel (model, Cmd.none)

            | _ -> ()
        with ex -> 
            App.program.onError("Error while restoring model found in app.Properties", ex)

    override this.OnStart() = 
        Console.WriteLine "OnStart: using same logic as OnResume()"
        this.OnResume()
//#endif


